package com.varuntravelsagency.customer.InterfaceUtil;

import com.varuntravelsagency.customer.ObjectUtil.RequestObject;

public interface ConnectionCallback {

    void onSuccess(Object data, RequestObject requestObject);

    void onError(String data, RequestObject requestObject);


}
