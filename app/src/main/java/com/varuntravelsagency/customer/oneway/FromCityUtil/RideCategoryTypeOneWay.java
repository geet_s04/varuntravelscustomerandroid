package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RideCategoryTypeOneWay implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("picture")
    @Expose
    private String picture;

    public RideCategoryTypeOneWay() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.tagline);
        dest.writeString(this.picture);
    }

    protected RideCategoryTypeOneWay(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.tagline = in.readString();
        this.picture = in.readString();
    }

    public static final Creator<RideCategoryTypeOneWay> CREATOR = new Creator<RideCategoryTypeOneWay>() {
        @Override
        public RideCategoryTypeOneWay createFromParcel(Parcel source) {
            return new RideCategoryTypeOneWay(source);
        }

        @Override
        public RideCategoryTypeOneWay[] newArray(int size) {
            return new RideCategoryTypeOneWay[size];
        }
    };

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
