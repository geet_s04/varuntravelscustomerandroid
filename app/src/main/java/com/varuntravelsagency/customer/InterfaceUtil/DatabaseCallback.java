package com.varuntravelsagency.customer.InterfaceUtil;

import android.net.Uri;

import com.varuntravelsagency.customer.ObjectUtil.RequestObject;

public interface DatabaseCallback {

    void onSuccess(Uri data, RequestObject requestObject);

    void onError(String data, RequestObject requestObject);

}
