package com.varuntravelsagency.customer.TextviewUtil;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.varuntravelsagency.customer.FontUtil.Font;

public class PrimeRegularTextview extends AppCompatTextView {
    public PrimeRegularTextview(Context context) {
        super(context);
        setFont(context);
    }

    public PrimeRegularTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public PrimeRegularTextview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context) {
        setTypeface(Font.prime_regular_font(context));
    }
}
