package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EstimatedFarePriceOneWay implements Parcelable {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("estimatedFare")
    @Expose
    private String estimatedFare;
    @SerializedName("estimatedDistance")
    @Expose
    private String estimatedDistance;

    public EstimatedFarePriceOneWay() {
    }

    public String getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(String estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getEstimatedDistance() {
        return estimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.estimatedFare);
        dest.writeString(this.estimatedDistance);
    }

    protected EstimatedFarePriceOneWay(Parcel in) {
        this.estimatedFare = in.readString();
        this.estimatedDistance = in.readString();
    }

    public static final Creator<EstimatedFarePriceOneWay> CREATOR = new Creator<EstimatedFarePriceOneWay>() {
        @Override
        public EstimatedFarePriceOneWay createFromParcel(Parcel source) {
            return new EstimatedFarePriceOneWay(source);
        }

        @Override
        public EstimatedFarePriceOneWay[] newArray(int size) {
            return new EstimatedFarePriceOneWay[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
