package com.varuntravelsagency.customer.oneway;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.varuntravelsagency.customer.ActivityUtil.AddBilling;
import com.varuntravelsagency.customer.AdapterUtil.RideTypeAdapter;
import com.varuntravelsagency.customer.ConstantUtil.Constant;
import com.varuntravelsagency.customer.CustomUtil.GlideApp;
import com.varuntravelsagency.customer.InterfaceUtil.ConnectionCallback;
import com.varuntravelsagency.customer.ManagementUtil.Management;
import com.varuntravelsagency.customer.ObjectUtil.DataObject;
import com.varuntravelsagency.customer.ObjectUtil.PrefObject;
import com.varuntravelsagency.customer.ObjectUtil.RequestObject;
import com.varuntravelsagency.customer.R;
import com.varuntravelsagency.customer.Utility.Utility;
import com.varuntravelsagency.customer.oneway.FromCityUtil.City;
import com.varuntravelsagency.customer.oneway.FromCityUtil.RideCategory;
import com.varuntravelsagency.customer.oneway.FromCityUtil.RideCategoryTypeOneWay;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OneWay extends AppCompatActivity implements View.OnClickListener, ConnectionCallback {
    private String TAG = OneWay.class.getName();
    private TextView txtMenu;
    private ImageView imageBack;
    private Management management;
    private PrefObject prefObject;
    private GeometricProgressView progressView;

    private CardView cardFromCity;
    private TextView txt_from_city_name;
    private ImageView image_ride_type;
    private TextView txt_ride_type_name;
    private TextView txtFairPrice;
    private TextView txtPaymentType;
    private LinearLayout layoutDone;
    private LinearLayout layoutPickUp;
    private LinearLayout layoutPaymentType;
    private CardView cardStart;
    private ImageView imagePayment;
    private String paymentCustomerNo;

    final ArrayList<Object> fromCityList = new ArrayList<>();
    final ArrayList<Object> cityFromCityIdList = new ArrayList<>();
    final ArrayList<Object> categoryList = new ArrayList<>();
    final ArrayList<Object> allRideCategoryTypeOneWayList = new ArrayList<>();
    final ArrayList<Object> paymentArraylist = new ArrayList<>();

    private CityFromCityIdAdapter cityFromCityIdAdapterAdapter;
    private String fromCityName;
    private String fromCityId;
    private String placeId;
    private String paymentType;
    private String paymentTypeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Utility.changeAppTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_way);

        initUI(); //Initialize UI
        getFromCityListFromServer();
        getAllRideCategoryFromServer();
//        getRidePaymentTypeFromServer();
    }


    /**
     * <p>It initialize the UI</p>
     */
    private void initUI() {

        management = new Management(this);

        txtMenu = findViewById(R.id.txt_menu);
        txtMenu.setText(Utility.getStringFromRes(this, R.string.one_way));

        imageBack = findViewById(R.id.image_back);
        progressView = findViewById(R.id.progressView);
        imageBack.setImageResource(R.drawable.ic_back);
        imageBack.setVisibility(View.VISIBLE);


        prefObject = management.getPreferences(new PrefObject()
                .setRetrieveUserCredential(true));

        cardFromCity = findViewById(R.id.card_from_city);
        txt_from_city_name = findViewById(R.id.txt_from_city_name);
        txt_ride_type_name = findViewById(R.id.txt_ride_type_name);
        image_ride_type = findViewById(R.id.image_ride_type);
        txtFairPrice = findViewById(R.id.txt_ride_price);
        layoutDone = findViewById(R.id.layout_done);
        layoutPickUp = findViewById(R.id.layout_pick_up);
        layoutPaymentType = findViewById(R.id.layout_payment_type);
        txtPaymentType = findViewById(R.id.txt_payment_type);
        imagePayment = findViewById(R.id.image_payment);
        paymentType = "cash";
        cardStart = findViewById(R.id.card_start);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerCityFromCityId = findViewById(R.id.recycler_view_city_from_city_id);
        recyclerCityFromCityId.setLayoutManager(gridLayoutManager);

        cityFromCityIdAdapterAdapter = new CityFromCityIdAdapter(this, cityFromCityIdList) {
            @Override
            public void onCityFromCityIdSelectionListener(int position) {
                City cityFromCityId = (City) cityFromCityIdList.get(position);
                Utility.Logger(TAG, "cityFromCityIdAdapterAdapter " + cityFromCityId.getName());
//                Utility.Toaster(OneWay.this, cityFromCityId.getName(), Toast.LENGTH_SHORT);
                placeId = cityFromCityId.getId();

                getAllRideCategoryTypeOneWayFromServer();
            }
        };
        recyclerCityFromCityId.setAdapter(cityFromCityIdAdapterAdapter);

        cardFromCity.setOnClickListener(this);
        imageBack.setOnClickListener(this);
        cardStart.setOnClickListener(this);
        layoutPaymentType.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == imageBack) {
            finish();
        }
        if (v == cardFromCity) {
            showFromCityBottomSheet(this);
        }

        if (v == cardStart) {

        }
        if (v == layoutPaymentType) {
            showPaymentTypeBottomSheet(OneWay.this, paymentType);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    /**
     * <p>It is used to show From City Selection</p>
     *
     * @param context
     */
    private void showFromCityBottomSheet(final Context context) {
        final View view = getLayoutInflater().inflate(R.layout.from_city_bottom_sheet, null);

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bottomSheetDialog.show();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerViewRideType = view.findViewById(R.id.recycler_view_from_city);
        recyclerViewRideType.setLayoutManager(gridLayoutManager);

        FromCityAdapter rideTypeAdapter = new FromCityAdapter(this, fromCityList) {
            @Override
            public void onFromCitySelectionListener(int position) {

                City riderTypeObject = (City) fromCityList.get(position);
                fromCityName = riderTypeObject.getName();
                fromCityId = riderTypeObject.getId();
                txt_from_city_name.setText(riderTypeObject.getName());

                getCityListFromCityIdFromServer();

                if (bottomSheetDialog.isShowing()) {
                    bottomSheetDialog.dismiss();
                }
            }
        };
        recyclerViewRideType.setAdapter(rideTypeAdapter);
    }

    /**
     * <p>It is used to convert Object into Json</p>
     *
     * @param
     * @return
     */


    private String getFromCityJson() {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "retrieve_base_location");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    @Override
    public void onSuccess(Object data, RequestObject requestObject) {
        if (data != null && requestObject != null) {
            if (data instanceof DataObject) {
                progressView.setVisibility(View.GONE);
                DataObject dataObject = (DataObject) data;
                Utility.Logger(TAG, requestObject.getConnection() + " onSuccess : " + dataObject);

                if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_BASE_LOCATION) {
                    Utility.Logger(TAG, "getFromCityListFromServer " + dataObject);
                    fromCityList.clear();
                    fromCityList.addAll(dataObject.getCityList());

                    if (!fromCityList.isEmpty()) {
                        City riderTypeObject = (City) fromCityList.get(0);
                        fromCityName = riderTypeObject.getName();
                        fromCityId = riderTypeObject.getId();
                        txt_from_city_name.setText(riderTypeObject.getName());

                        getCityListFromCityIdFromServer();
                    }
                } else if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_ALL_RIDE_CATEGORY) {
                    Utility.Logger(TAG, "getAllRideCategoryFromServer " + dataObject.getCategoryList());
                    categoryList.clear();
                    categoryList.addAll(dataObject.getCategoryList());
                } else if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_CITY_BY_BASE) {
                    Utility.Logger(TAG, "getCityListFromCityIdFromServer " + dataObject.getCityList());
                    cityFromCityIdList.clear();
                    cityFromCityIdList.addAll(dataObject.getCityList());
                    cityFromCityIdAdapterAdapter.notifyDataSetChanged();
                } else if (requestObject.getConnection() == Constant.CONNECTION.FIND_ESTIMATED_FARE_PRICE_ONE_WAY) {
                    Utility.Logger(TAG, "getAllRideCategoryTypeOneWayFromServer onSuccess " + dataObject.getCountryList().size());

                    txtFairPrice.setText("₹ " + dataObject.getEstimatedFare());
                    txtFairPrice.setVisibility(View.VISIBLE);
                    layoutDone.setVisibility(View.VISIBLE);
                    layoutPickUp.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onError(String data, RequestObject requestObject) {

        if (!Utility.isEmptyString(data) && requestObject != null) {
            Utility.Logger(TAG, "Error = " + data);
            progressView.setVisibility(View.GONE);
            if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_BASE_LOCATION) {
                Utility.Toaster(OneWay.this, data, Toast.LENGTH_SHORT);
            } else if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_ALL_RIDE_CATEGORY) {
                Utility.Toaster(OneWay.this, data, Toast.LENGTH_SHORT);
            } else if (requestObject.getConnection() == Constant.CONNECTION.RETRIEVE_CITY_BY_BASE) {
                Utility.Toaster(OneWay.this, data, Toast.LENGTH_SHORT);
            } else if (requestObject.getConnection() == Constant.CONNECTION.FIND_ESTIMATED_FARE_PRICE_ONE_WAY) {
                Utility.Toaster(OneWay.this, data, Toast.LENGTH_SHORT);
                txtFairPrice.setText("");
                txtFairPrice.setVisibility(View.GONE);
                layoutDone.setVisibility(View.GONE);
                layoutPickUp.setVisibility(View.VISIBLE);
            }
        }
    }

    private void getFromCityListFromServer() {
        progressView.setVisibility(View.VISIBLE);
        management.sendRequestToServer(new RequestObject()
                .setContext(this)
                .setJson(getFromCityJson())
                .setConnection(Constant.CONNECTION.RETRIEVE_BASE_LOCATION)
                .setConnectionType(Constant.CONNECTION_TYPE.UI)
                .setConnectionCallback(this));
    }

    private String getAllRideCategoryJson() {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "retrieve_all_ride_category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    private void getAllRideCategoryFromServer() {
        management.sendRequestToServer(new RequestObject()
                .setContext(this)
                .setJson(getAllRideCategoryJson())
                .setConnection(Constant.CONNECTION.RETRIEVE_ALL_RIDE_CATEGORY)
                .setConnectionType(Constant.CONNECTION_TYPE.UI)
                .setConnectionCallback(this));
    }

    private String getRidePaymentTypeJson(String latitude, String longitude, String city, String user_id) {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "retrieve_ride_type");
            jsonObject.accumulate("latitude", latitude);
            jsonObject.accumulate("longitude", longitude);
            jsonObject.accumulate("city", city);
            jsonObject.accumulate("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    private void getRidePaymentTypeFromServer() {
        management.sendRequestToServer(new RequestObject()
                .setContext(OneWay.this)
                .setJson(getRidePaymentTypeJson("", "", "", prefObject.getUserId()))
                .setConnection(Constant.CONNECTION.RETRIEVE_RIDE_PAYMENT_TYPE)
                .setConnectionType(Constant.CONNECTION_TYPE.UI)
                .setConnectionCallback(this));
    }

    private String getCityListFromCityIdJson(String city_id) {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "retrieve_city_by_base");
            jsonObject.accumulate("city_id", city_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    private void getCityListFromCityIdFromServer() {
        management.sendRequestToServer(new RequestObject()
                .setContext(this)
                .setJson(getCityListFromCityIdJson(fromCityId))
                .setConnection(Constant.CONNECTION.RETRIEVE_CITY_BY_BASE)
                .setConnectionType(Constant.CONNECTION_TYPE.UI)
                .setConnectionCallback(this));
    }

    private String getAllRideCategoryTypeOneWayJson(String place_id, String ride_type_id) {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "retrieve_all_ride_category_type_one_way");
            jsonObject.accumulate("place_id", place_id);
            jsonObject.accumulate("ride_type_id", ride_type_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    private void getAllRideCategoryTypeOneWayFromServer() {
        progressView.setVisibility(View.VISIBLE);
        allRideCategoryTypeOneWayList.clear();
        for (int i = 0; i < categoryList.size(); i++) {
            final int finalI = i;
            RideCategory rideCategory = ((RideCategory) categoryList.get(i));
            Utility.Logger(TAG, "getAllRideCategoryTypeOneWayFromServer " + rideCategory.getName());
            management.sendRequestToServer(new RequestObject()
                    .setContext(this)
                    .setJson(getAllRideCategoryTypeOneWayJson(placeId, rideCategory.getId()))
                    .setConnection(Constant.CONNECTION.RETRIEVE_ALL_RIDE_CATEGORY_TYPE_ONE_WAY)
                    .setConnectionType(Constant.CONNECTION_TYPE.UI)
                    .setConnectionCallback(new ConnectionCallback() {
                        @Override
                        public void onSuccess(Object data, RequestObject requestObject) {
                            DataObject couponObject = (DataObject) data;
                            Utility.Logger(TAG, "getAllRideCategoryTypeOneWayFromServer onSuccess " + couponObject.getCountryList().size());
                            allRideCategoryTypeOneWayList.addAll(couponObject.getCountryList());
                            if (finalI == categoryList.size() - 1) {
                                progressView.setVisibility(View.GONE);
                                Utility.Logger(TAG, "getAllRideCategoryTypeOneWayFromServer size " + allRideCategoryTypeOneWayList.size());
                                showAllRideCategoryTypeOneWayBottomSheet(OneWay.this);
                            }
                        }

                        @Override
                        public void onError(String data, RequestObject requestObject) {
                            Utility.Logger(TAG, data);
                            if (finalI == categoryList.size() - 1) {
                                progressView.setVisibility(View.GONE);
                                Utility.Logger(TAG, "getAllRideCategoryTypeOneWayFromServer size " + allRideCategoryTypeOneWayList.size());
                                showAllRideCategoryTypeOneWayBottomSheet(OneWay.this);
                            }
                        }
                    }));
        }
    }

    private void showAllRideCategoryTypeOneWayBottomSheet(final Context context) {
        final View view = getLayoutInflater().inflate(R.layout.all_ride_category_type_one_way_bottom_sheet, null);

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bottomSheetDialog.show();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerViewRideType = view.findViewById(R.id.recycler_view_all_ride_category_type_one_way);
        recyclerViewRideType.setLayoutManager(gridLayoutManager);

        AllRideCategoryTypeOneWayAdapter rideTypeAdapter = new AllRideCategoryTypeOneWayAdapter(this, allRideCategoryTypeOneWayList) {
            @Override
            public void onRideCategoryTypeOneWaySelectionListener(int position) {

                RideCategoryTypeOneWay riderTypeObject = (RideCategoryTypeOneWay) allRideCategoryTypeOneWayList.get(position);
//                Utility.Toaster(OneWay.this, riderTypeObject.getName() + " Selected", Toast.LENGTH_SHORT);

                GlideApp.with(OneWay.this).load(Constant.ServerInformation.RIDE_TYPE_URL + riderTypeObject.getPicture())
                        .into(image_ride_type);
                txt_ride_type_name.setText(riderTypeObject.getName());

                getEstimatedFarePriceOneWayFromServer(riderTypeObject.getId());
                if (bottomSheetDialog.isShowing()) {
                    bottomSheetDialog.dismiss();
                }
            }
        };
        recyclerViewRideType.setAdapter(rideTypeAdapter);
    }


    private String getEstimatedFarePriceOneWayJson(String ride_type_id) {
        String json = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("functionality", "find_estimated_fare_price_one_way");
            jsonObject.accumulate("ride_type_id", ride_type_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonObject.toString();
        Utility.Logger(TAG, "JSON " + json);
        return json;
    }

    private void getEstimatedFarePriceOneWayFromServer(String ride_type_id) {
        progressView.setVisibility(View.VISIBLE);
        Utility.Logger(TAG, "getEstimatedFarePriceOneWayFromServer " + ride_type_id);
        management.sendRequestToServer(new RequestObject()
                .setContext(this)
                .setJson(getEstimatedFarePriceOneWayJson(ride_type_id))
                .setConnection(Constant.CONNECTION.FIND_ESTIMATED_FARE_PRICE_ONE_WAY)
                .setConnectionType(Constant.CONNECTION_TYPE.UI)
                .setConnectionCallback(this));
    }

    /**
     * <p>It is used to show Payment Selection</p>
     *
     * @param context
     */
    private void showPaymentTypeBottomSheet(final Context context, final String payment_type) {
        final View view = getLayoutInflater().inflate(R.layout.ride_type_bottom_sheet, null);

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bottomSheetDialog.show();

        for (int i = 0; i < paymentArraylist.size(); i++) {

            DataObject rideObject = (DataObject) paymentArraylist.get(i);

            if (rideObject.getDataType() == Constant.DATA_TYPE.ADD_CARD) {
                continue;
            }

            if (rideObject.getPayment_type().equalsIgnoreCase(payment_type))
                rideObject.setSelected_payment(true);
            else
                rideObject.setSelected_payment(false);
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(OneWay.this, 1, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerViewRideType = view.findViewById(R.id.recycler_view_ride_type);
        recyclerViewRideType.setLayoutManager(gridLayoutManager);

        RideTypeAdapter rideTypeAdapter = new RideTypeAdapter(OneWay.this, paymentArraylist) {
            @Override
            public void onRideTypeSelectionListener(int position) {

                DataObject riderTypeObject = (DataObject) paymentArraylist.get(position);

                if (riderTypeObject.getDataType() == Constant.DATA_TYPE.ADD_CARD) {
                    Utility.Logger(TAG, "Adding Card Functionality");

                    Intent intent = new Intent(context, AddBilling.class);
                    startActivityForResult(intent, Constant.RequestCode.REQUEST_CODE_ADD_CARD);

                    if (bottomSheetDialog.isShowing()) {
                        bottomSheetDialog.dismiss();
                    }

                    return;
                }

                paymentType = riderTypeObject.getPayment_type();

                Utility.Logger(TAG, "Payment Type = " + paymentType + " Payment " + riderTypeObject.getPayment_type());

                if (riderTypeObject.getDataType() == Constant.DATA_TYPE.CARD_TYPE) {

                    txtPaymentType.setText(riderTypeObject.getPayment_type_name());

                } else if (riderTypeObject.getDataType() == Constant.DATA_TYPE.CARD_DETAIL) {

                    txtPaymentType.setText(Utility.maskSomeCharacter(riderTypeObject.getPayment_card_no()));
                    paymentCustomerNo = riderTypeObject.getStripe_customer_no();

                }

                GlideApp.with(context).load(Constant.ServerInformation.PAYMENT_URL + riderTypeObject.getPayment_type_picture())
                        .into(imagePayment);

                if (bottomSheetDialog.isShowing()) {
                    bottomSheetDialog.dismiss();
                }
            }
        };
        recyclerViewRideType.setAdapter(rideTypeAdapter);
    }
}
