package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("from_city")
    @Expose
    private String fromCity;
    @SerializedName("to_city")
    @Expose
    private String toCity;
    @SerializedName("to_city_name")
    @Expose
    private String toCityName;
    @SerializedName("price")
    @Expose
    private String price;
//    @SerializedName("product_attribute")
//    @Expose
//    private List<ProductAttribute> productAttribute = null;

    public City() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

//    public List<ProductAttribute> getProductAttribute() {
//        return productAttribute;
//    }
//
//    public void setProductAttribute(List<ProductAttribute> productAttribute) {
//        this.productAttribute = productAttribute;
//    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.fromCity);
        dest.writeString(this.toCity);
        dest.writeString(this.price);
        dest.writeString(this.toCityName);
//        dest.writeTypedList(this.productAttribute);
    }

    protected City(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.fromCity = in.readString();
        this.toCity = in.readString();
        this.price = in.readString();
        this.toCityName = in.readString();
//        this.productAttribute = in.createTypedArrayList(ProductAttribute.CREATOR);
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getToCityName() {
        return toCityName;
    }

    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }
}
