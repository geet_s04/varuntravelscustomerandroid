package com.varuntravelsagency.customer.oneway;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.varuntravelsagency.customer.ObjectUtil.EmptyObject;
import com.varuntravelsagency.customer.ObjectUtil.ProgressObject;
import com.varuntravelsagency.customer.R;
import com.varuntravelsagency.customer.oneway.FromCityUtil.City;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.ArrayList;


/**
 * Created by hp on 5/5/2018.
 */

public abstract class FromCityAdapter extends RecyclerView.Adapter {
    private int NO_DATA_VIEW = 1;
    private int PROGRESS_VIEW = 2;
    private int FROM_CITY_VIEW = 3;

    private Context context;
    private ArrayList<Object> dataArray = new ArrayList<>();


    public FromCityAdapter(Context context, ArrayList<Object> dataArray) {
        this.context = context;
        this.dataArray = dataArray;

    }

    @Override
    public int getItemViewType(int position) {


        if (dataArray.get(position) instanceof EmptyObject) {
            return NO_DATA_VIEW;
        } else if (dataArray.get(position) instanceof City) {
            City dataObject = (City) dataArray.get(position);
            return FROM_CITY_VIEW;

        } else if (dataArray.get(position) instanceof ProgressObject) {
            return PROGRESS_VIEW;
        }

        return NO_DATA_VIEW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        if (viewType == NO_DATA_VIEW) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item_layout, parent, false);
            viewHolder = new EmptyHolder(view);

        } else if (viewType == PROGRESS_VIEW) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item_layout, parent, false);
            viewHolder = new ProgressHolder(view);

        } else if (viewType == FROM_CITY_VIEW) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.from_city_item_layout, parent, false);
            viewHolder = new FromCityHolder(view);

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final StaggeredGridLayoutManager.LayoutParams layoutParams =
                new StaggeredGridLayoutManager.LayoutParams(
                        holder.itemView.getLayoutParams());

        if (holder instanceof ProgressHolder) {

            ProgressHolder lookUpHolder = (ProgressHolder) holder;
            layoutParams.setFullSpan(true);

        } else if (holder instanceof EmptyHolder) {

            EmptyHolder emptyHolder = (EmptyHolder) holder;
            EmptyObject emptyState = (EmptyObject) dataArray.get(position);

            emptyHolder.imageIcon.setImageResource(emptyState.getPlaceHolderIcon());
            emptyHolder.txtTitle.setText(emptyState.getTitle());
            emptyHolder.txtDescription.setText(emptyState.getDescription());

            layoutParams.setFullSpan(true);

        } else if (holder instanceof FromCityHolder) {

            City rideTypeObject = (City) dataArray.get(position);
            final FromCityHolder rideTypeHolder = (FromCityHolder) holder;

            rideTypeHolder.txtFromCity.setText(rideTypeObject.getName());

            rideTypeHolder.txtFromCity.setTag(position);
            rideTypeHolder.layout_from_city.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) rideTypeHolder.txtFromCity.getTag();
                    onFromCitySelectionListener(pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.size();

    }

    public abstract void onFromCitySelectionListener(int position);


    protected class EmptyHolder extends RecyclerView.ViewHolder {
        private ImageView imageIcon;
        private TextView txtTitle;
        private TextView txtDescription;

        public EmptyHolder(View view) {
            super(view);

            imageIcon = view.findViewById(R.id.image_icon);
            txtTitle = view.findViewById(R.id.txt_title);
            txtDescription = view.findViewById(R.id.txt_description);
        }
    }

    protected class ProgressHolder extends RecyclerView.ViewHolder {
        private GeometricProgressView progressView;

        public ProgressHolder(View view) {
            super(view);
            progressView = view.findViewById(R.id.progressView);
        }

    }

    protected class FromCityHolder extends RecyclerView.ViewHolder {
        private LinearLayout layout_from_city;
        private TextView txtFromCity;

        public FromCityHolder(View view) {
            super(view);

            layout_from_city = view.findViewById(R.id.layout_from_city);
            txtFromCity = view.findViewById(R.id.txtFromCity);
        }
    }
}
