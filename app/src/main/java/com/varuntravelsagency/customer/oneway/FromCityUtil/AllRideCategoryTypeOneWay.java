package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllRideCategoryTypeOneWay implements Parcelable {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country_list")
    @Expose
    private List<RideCategoryTypeOneWay> countryList = null;

    public AllRideCategoryTypeOneWay() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(this.countryList);
    }

    protected AllRideCategoryTypeOneWay(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.countryList = in.createTypedArrayList(RideCategoryTypeOneWay.CREATOR);
    }

    public static final Creator<AllRideCategoryTypeOneWay> CREATOR = new Creator<AllRideCategoryTypeOneWay>() {
        @Override
        public AllRideCategoryTypeOneWay createFromParcel(Parcel source) {
            return new AllRideCategoryTypeOneWay(source);
        }

        @Override
        public AllRideCategoryTypeOneWay[] newArray(int size) {
            return new AllRideCategoryTypeOneWay[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RideCategoryTypeOneWay> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<RideCategoryTypeOneWay> countryList) {
        this.countryList = countryList;
    }
}
