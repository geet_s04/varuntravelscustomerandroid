package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RideCategory implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    public RideCategory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
    }

    protected RideCategory(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
    }

    public static final Creator<RideCategory> CREATOR = new Creator<RideCategory>() {
        @Override
        public RideCategory createFromParcel(Parcel source) {
            return new RideCategory(source);
        }

        @Override
        public RideCategory[] newArray(int size) {
            return new RideCategory[size];
        }
    };
}
