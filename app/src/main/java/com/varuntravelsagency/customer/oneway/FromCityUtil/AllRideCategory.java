package com.varuntravelsagency.customer.oneway.FromCityUtil;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllRideCategory implements Parcelable {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category_list")
    @Expose
    private List<RideCategory> categoryList = null;

    @SerializedName("from_city")
    @Expose
    private String fromCity;
    @SerializedName("to_city")
    @Expose
    private String toCity;
    @SerializedName("price")
    @Expose
    private String price;
//    @SerializedName("product_attribute")
//    @Expose
//    private List<ProductAttribute> productAttribute = null;

    public AllRideCategory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

//    public List<ProductAttribute> getProductAttribute() {
//        return productAttribute;
//    }
//
//    public void setProductAttribute(List<ProductAttribute> productAttribute) {
//        this.productAttribute = productAttribute;
//    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.fromCity);
        dest.writeString(this.toCity);
        dest.writeString(this.price);
        dest.writeTypedList(this.categoryList);
    }

    protected AllRideCategory(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.fromCity = in.readString();
        this.toCity = in.readString();
        this.price = in.readString();
        this.categoryList = in.createTypedArrayList(RideCategory.CREATOR);
    }

    public static final Creator<AllRideCategory> CREATOR = new Creator<AllRideCategory>() {
        @Override
        public AllRideCategory createFromParcel(Parcel source) {
            return new AllRideCategory(source);
        }

        @Override
        public AllRideCategory[] newArray(int size) {
            return new AllRideCategory[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RideCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<RideCategory> categoryList) {
        this.categoryList = categoryList;
    }
}
